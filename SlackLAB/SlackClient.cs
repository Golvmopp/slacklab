﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SlackLAB
{
    class SlackClient
    {
        private readonly Uri _uri;
        private readonly Encoding _encoding = new UTF8Encoding();

        public SlackClient(string urlWithAccessToken)
        {
            _uri = new Uri(urlWithAccessToken);
        }

        public void PostMessage(string text, string username, string channel)
        {
            Payload payload = new Payload()
            {
                Channel = channel,
                Username = username,
                Text = text
            };
            PostMessage(payload);
        }

        public void PostMessage(Payload payload)
        {
            string payloadJson = JsonConvert.SerializeObject(payload);

            using (WebClient client = new WebClient())
            {
                NameValueCollection data = new NameValueCollection();
                data["payload"] = payloadJson;
                String responseText = "";
                try
                {
                    var response = client.UploadValues(_uri, "POST", data);
                    responseText = _encoding.GetString(response);
                    Debug.WriteLine("try-responseText: " + responseText);
                }
                catch (WebException we)
                {
                    responseText = ((HttpWebResponse)we.Response).StatusCode.ToString();
                    Debug.WriteLine("catch-responseText: " + responseText);
                }
            }
        }
    }

    public class Payload
	{
	    [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
