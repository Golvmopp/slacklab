﻿namespace SlackLAB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Send = new System.Windows.Forms.Button();
            this.txtbox_Message = new System.Windows.Forms.TextBox();
            this.txtbox_Name = new System.Windows.Forms.TextBox();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_Message = new System.Windows.Forms.Label();
            this.label_Send = new System.Windows.Forms.Label();
            this.label_Time = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Send
            // 
            this.btn_Send.Location = new System.Drawing.Point(13, 226);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(75, 23);
            this.btn_Send.TabIndex = 0;
            this.btn_Send.Text = "Send";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // txtbox_Message
            // 
            this.txtbox_Message.Location = new System.Drawing.Point(13, 108);
            this.txtbox_Message.Name = "txtbox_Message";
            this.txtbox_Message.Size = new System.Drawing.Size(100, 20);
            this.txtbox_Message.TabIndex = 1;
            // 
            // txtbox_Name
            // 
            this.txtbox_Name.Location = new System.Drawing.Point(13, 42);
            this.txtbox_Name.Name = "txtbox_Name";
            this.txtbox_Name.Size = new System.Drawing.Size(100, 20);
            this.txtbox_Name.TabIndex = 2;
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Location = new System.Drawing.Point(13, 23);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(35, 13);
            this.label_Name.TabIndex = 3;
            this.label_Name.Text = "Name";
            // 
            // label_Message
            // 
            this.label_Message.AutoSize = true;
            this.label_Message.Location = new System.Drawing.Point(13, 89);
            this.label_Message.Name = "label_Message";
            this.label_Message.Size = new System.Drawing.Size(50, 13);
            this.label_Message.TabIndex = 4;
            this.label_Message.Text = "Message";
            // 
            // label_Send
            // 
            this.label_Send.AutoSize = true;
            this.label_Send.Location = new System.Drawing.Point(13, 207);
            this.label_Send.Name = "label_Send";
            this.label_Send.Size = new System.Drawing.Size(0, 13);
            this.label_Send.TabIndex = 5;
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Location = new System.Drawing.Point(222, 22);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(30, 13);
            this.label_Time.TabIndex = 6;
            this.label_Time.Text = "Time";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label_Time);
            this.Controls.Add(this.label_Send);
            this.Controls.Add(this.label_Message);
            this.Controls.Add(this.label_Name);
            this.Controls.Add(this.txtbox_Name);
            this.Controls.Add(this.txtbox_Message);
            this.Controls.Add(this.btn_Send);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Send;
        private System.Windows.Forms.TextBox txtbox_Message;
        private System.Windows.Forms.TextBox txtbox_Name;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_Message;
        private System.Windows.Forms.Label label_Send;
        private System.Windows.Forms.Label label_Time;
    }
}

